// Generated on 2014-10-22 using
// generator-webapp 0.5.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Configurable paths
  var config = {
    app: 'app',
    dist: 'dist',
    currentSlide: 'slide-00',
    currentSlideID: null,  // Set by __set-slide-id task
    distSlideDir: '<%= config.dist %>/slides/<%= config.currentSlide %>'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      js: {
        files: ['<%= config.app %>/scripts/{,*/}*.js'],
        tasks: ['concat:corejs', 'jshint'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jstest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['test:watch']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      sass: {
        files: ['<%= config.app %>/styles/{,*/}*.{scss,sass}'],
        tasks: ['sass:server', 'autoprefixer']
      },
      styles: {
        files: ['<%= config.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'autoprefixer']
      }//,
      //livereload: {
      //  options: {
      //    livereload: '<%= connect.options.livereload %>'
      //  },
      //  files: [
      //    '<%= config.app %>/{,*/}*.html',
      //    '.tmp/styles/{,*/}*.css',
      //    '<%= config.app %>/images/{,*/}*'
      //  ]
      //}
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        open: true,
        livereload: 35729,
        hostname: '127.0.0.1'
      },
      livereload: {
        options: {
          middleware: function(connect) {
            return [
              connect.static('.tmp'),
              connect().use('/bower_components', connect.static('./bower_components')),
              connect.static(config.app)
            ];
          }
        }
      },
      test: {
        options: {
          open: false,
          port: 9001,
          middleware: function(connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use('/bower_components', connect.static('./bower_components')),
              connect.static(config.app)
            ];
          }
        }
      },
      dist: {
        options: {
          base: '<%= config.dist %>',
          livereload: false
        }
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      slide: '<%= config.distSlideDir %>',
      slideParameters: '<%= config.distSlideDir %>/parameters',
      server: '.tmp'
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= config.app %>/scripts/{,*/}*.js',
        '!<%= config.app %>/scripts/vendor/*',
        'test/spec/{,*/}*.js'
      ]
    },

    // Mocha testing framework configuration options
    mocha: {
      all: {
        options: {
          run: true,
          urls: ['http://<%= connect.test.options.hostname %>:<%= connect.test.options.port %>/index.html']
        }
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
        loadPath: 'bower_components'
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles',
          src: ['*.{scss,sass}'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      },
      server: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles',
          src: ['*.{scss,sass}'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      }
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Renames files for browser caching purposes
    rev: {
      slide: {
        files: {
          src: [
            '<%= config.distSlideDir %>/js/{,*/}*.js',
            '<%= config.distSlideDir %>/css/{,*/}*.css',
            '<%= config.distSlideDir %>/media/fonts/{,*/}*.*',
            '<%= config.distSlideDir %>/media/images/{,*/}*.*',
            '!<%= config.distSlideDir %>/media/images/thumbnails/{,*/}*.*',
            '<%= config.distSlideDir %>/media/video/{,*/}*.*'
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      options: {
        dest: '<%= config.distSlideDir %>'
      },
      slide: {
        src: '<%= config.app %>/<%= config.currentSlide %>.html'
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      html: ['<%= config.distSlideDir %>/{,*/}*.html'],
      css: ['<%= config.distSlideDir %>/css/{,*/}*.css']
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      slide: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/media/images/<%= config.currentSlide %>',
          src: '{,*/}*.{gif,jpeg,jpg,png,svg}',
          dest: '<%= config.distSlideDir %>/media/images/<%= config.currentSlide %>'
        }, {
          expand: true,
          cwd: '<%= config.app %>/media/images',
          src: '*.{gif,jpeg,jpg,png,svg}',
          dest: '<%= config.distSlideDir %>/media/images'
        }]
      }
    },

    svgmin: {
      slide: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/media/images/<%= config.currentSlide %>',
          src: '{,*/}*.svg',
          dest: '<%= config.distSlideDir %>/media/images/<%= config.currentSlide %>'
        }]
      }
    },

    // htmlmin: {
    //   dist: {
    //     options: {
    //       collapseBooleanAttributes: true,
    //       collapseWhitespace: true,
    //       conservativeCollapse: true,
    //       removeAttributeQuotes: true,
    //       removeCommentsFromCDATA: true,
    //       removeEmptyAttributes: true,
    //       removeOptionalTags: true,
    //       removeRedundantAttributes: true,
    //       useShortDoctype: true
    //     },
    //     files: [{
    //       expand: true,
    //       cwd: '<%= config.dist %>',
    //       src: '{,*/}*.html',
    //       dest: '<%= config.dist %>'
    //     }]
    //   }
    // },

    // By default, your `index.html`'s <!-- Usemin block --> will take care
    // of minification. These next options are pre-configured if you do not
    // wish to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= config.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css',
    //         '<%= config.app %>/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= config.dist %>/scripts/scripts.js': [
    //         '<%= config.dist %>/scripts/scripts.js'
    //       ]
    //     }
    //   }
    // },

    concat: {
      //dist: {},
      corejs: {
        options: {
          separator: ';',
          stripBanners: true
        },
        files: [
          {
            dest: '.tmp/scripts/core.js',
            src: [
              'bower_components/jquery/dist/jquery.js',
              'bower_components/fastclick/lib/fastclick.js',
              'bower_components/magnific-popup/dist/jquery.magnific-popup.min.js',
              'bower_components/hammer.js/hammer.js',
              'bower_components/jquery.hammer.js/jquery.hammer.js',
              '<%= config.app %>/scripts/cegedim.js',
              '<%= config.app %>/scripts/flow.js',
              '<%= config.app %>/scripts/core.js'
            ],

            nonull: true
          }
        ]
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      slide: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>',
          src: 'media/fonts/{,*/}*.*',
          dest: '<%= config.distSlideDir %>'
        }, {
          // index.html
          src: '<%= config.app %>/<%= config.currentSlide %>.html',
          dest: '<%= config.distSlideDir %>/index.html'
        },

        // Media files

        {
          expand: true,
          cwd: '<%= config.app %>',
          src: [
            'media/videos/<%= config.currentSlide %>/**/*',
            'media/pdf/<%= config.currentSlide %>/**/*'
          ],
          dest: '<%= config.distSlideDir %>'
        }, {
          expand: true,
          cwd: '<%= config.app %>/media/pdf',
          src: '*.pdf',
          dest: '<%= config.distSlideDir %>/media/pdf'
        },

        // Assets.

        {nonull: true, src: './assets/thumbnails/<%= config.currentSlide %>.jpg', dest: '<%= config.distSlideDir %>/media/images/thumbnails/200x150.jpg'},
        {nonull: true, src: './assets/export/<%= config.currentSlide %>.pdf', dest: '<%= config.distSlideDir %>/export/export.pdf'},

        ]
      },
      slideParameters: {
        files: [
          {nonull: true, src: './assets/parameters/<%= config.currentSlideID %>.xml', dest: '<%= config.distSlideDir %>/parameters/parameters.xml'},
        ]
      },
      styles: {
        expand: true,
        dot: true,
        cwd: '<%= config.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up build process
    concurrent: {
      server: [
        'sass:server',
        'copy:styles',
        'concat:corejs'
      ],
      test: [
        'copy:styles'
      ]
    },

    compress: {
      slide: {
        options: {
          archive: '<%= config.dist %>/slides/<%= config.currentSlideID %>.zip'
        },
        files: [{
          expand: true,
          cwd: '<%= config.distSlideDir %>',
          src: '**/*'
        }]
      }
    }
  });

  grunt.registerTask('serve', 'start the server and preview your app, --allow-remote for remote access', function (target) {
    if (grunt.option('allow-remote')) {
      grunt.config.set('connect.options.hostname', '192.168.0.54');
    }
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'concurrent:server',
      'autoprefixer',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);

  grunt.registerTask('test', function (target) {
    if (target !== 'watch') {
      grunt.task.run([
        'clean:server',
        'concurrent:test',
        'autoprefixer'
      ]);
    }

    grunt.task.run([
      'connect:test',
      'mocha'
    ]);
  });

  // slides description
  require('./buildfile').init(grunt, 're_kzaaa000000a', {
    'slide-00': ['0_0_Cover']
  });
};
