
module.exports.init = function(grunt, slidePrefix, slides) {
  var slug = require('slug'),
      fs = require('fs');

  grunt.registerTask('build', function() {
    var tasks = ['clean:dist'];

    Object.keys(slides).forEach(function(slideName) {
      tasks.push('__build-slide:' + slideName);
    });

    grunt.task.run(tasks);
  });

  grunt.registerTask('build-compress', function() {
    var tasks = [];

    Object.keys(slides).forEach(function(slideName) {
      tasks.push('__compress-slide:' + slideName);
    });

    grunt.task.run(tasks);
  });

  grunt.registerTask('build-all', function() {
    grunt.task.run(['build', 'generate-parameters', 'build-compress']);
  });

  grunt.registerTask('build-slide', function(slideName) {
    grunt.task.run([
      '__build-slide:' + slideName,
      'generate-parameters',
      '__compress-slide:' + slideName
    ]);
  });

  grunt.registerTask('generate-parameters', function() {
    Object.keys(slides).forEach(function(slideName) {
      for (var i = 0, l = slides[slideName].length; i < l; i++) {
        var slideID = getSlideID(slides[slideName][i]),
            fileName = __dirname + '/assets/parameters/' + slideID + '.xml';
        if (!fs.existsSync(fileName)) {
          fs.writeFileSync(fileName,
                           '<?xml version="1.0" encoding="UTF-8"?>\n' +
                           '<Sequence Id="' + slideID +'" xmlns="urn:param-schema">\n' +
                           '</Sequence>');
        }
      }
    });
  });

  grunt.registerTask('__build-slide', function(slideName) {
    grunt.config.set('config.currentSlide', slideName);
    grunt.config.set('concat.generated.files', []);
    grunt.config.set('uglify.generated.files', []);
    grunt.config.set('cssmin.generated.files', []);
    grunt.task.run([
      'clean:slide',
      'sass',
      'copy:styles',
      'autoprefixer',
      'useminPrepare:slide',
      'imagemin',
      'svgmin',
      'concat',
      'cssmin',
      'uglify',
      'copy:slide',
      'rev:slide',
      'usemin',
      // 'htmlmin',
    ]);
  });

  grunt.registerTask('__compress-slide', function(slideName) {
    var tasks = [];
    grunt.config.set('config.currentSlide', slideName);

    for (var i = 0, l = slides[slideName].length; i < l; i++) {
      tasks.push('__set-current-slide-id:' + getSlideID(slides[slideName][i]));
      tasks.push('copy:slideParameters');
      tasks.push('compress:slide');
      tasks.push('clean:slideParameters');
    }

    grunt.task.run(tasks);
  });

  grunt.registerTask('__set-current-slide-id', function(slideID) {
    grunt.config.set('config.currentSlideID', slideID);
  });

  function getSlideID(slideName) {
    return slug(slidePrefix + '_' + slideName).replace(/-/gi, '_').toUpperCase();
  }
};
